package dev.ladajaroslaw.projekt.controller;

import dev.ladajaroslaw.projekt.repository.ArticleRepository;
import dev.ladajaroslaw.projekt.searchlib.models.Article;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/articles")
public class ArticleController {

    private final ArticleRepository articleRepository;

    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @GetMapping("/keys")
    public List<Article> searchByKey(@RequestParam String key,
                                     @RequestParam(required = false, defaultValue = "false") Boolean withTypo,
                                     @RequestParam(value = "author", required = false) String authorField,
                                     @RequestParam(value = "category", required = false) String categoryField) throws SolrServerException, IOException {
        String[] keys = key.split(" ");
        if (keys.length > 1) return articleRepository.findByKeys(withTypo, authorField, categoryField, keys);
        return articleRepository.findByKey(withTypo, authorField, categoryField, key);
    }

    @GetMapping("/phrase")
    public List<Article> searchByPhrase(@RequestParam String phrase,
                                        @RequestParam(required = false, defaultValue = "false") Boolean proximity,
                                        @RequestParam(value = "author", required = false) String authorField,
                                        @RequestParam(value = "category", required = false) String categoryField) throws SolrServerException, IOException {
        return articleRepository.findByPhrase(proximity, authorField, categoryField, phrase);
    }

    @GetMapping("/stats/phrase")
    public Map<String, Long> statsByPhrase(@RequestParam String phrase,
                                           @RequestParam String statsField,
                                           @RequestParam(required = false, defaultValue = "false") Boolean proximity,
                                           @RequestParam(value = "author", required = false) String authorField,
                                           @RequestParam(value = "category", required = false) String categoryField) throws SolrServerException, IOException {
        return articleRepository.statsByPhrase(proximity, statsField, authorField, categoryField, phrase);
    }
}
