package dev.ladajaroslaw.projekt.repository;

import dev.ladajaroslaw.projekt.searchlib.models.Article;
import dev.ladajaroslaw.projekt.searchlib.search.ArticleSearch;
import dev.ladajaroslaw.projekt.searchlib.search.ExactFieldCondition;
import org.apache.solr.client.solrj.SolrServerException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component
public class ArticleRepository {

    private final String AUTHOR_FIELD = "author";
    private final String CATEGORY_FIELD = "category";

    public List<Article> findByKey(Boolean isFuzzy, String author, String category, String key) throws SolrServerException, IOException {
        if (isFuzzy) {
            return new ArticleSearch().turnOnType()
                    .simpleSearch(key)
                    .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                    .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                    .search();
        }
        return new ArticleSearch().simpleSearch(key)
                .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                .search();
    }

    public List<Article> findByKeys(Boolean isFuzzy, String author, String category, String... keys) throws SolrServerException, IOException {
        if (isFuzzy) {
            return new ArticleSearch().turnOnType()
                    .simpleSearch(keys)
                    .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                    .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                    .search();
        }
        return new ArticleSearch().simpleSearch(keys)
                .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                .search();
    }

    public List<Article> findByPhrase(Boolean isProximity, String author, String category, String phrase) throws SolrServerException, IOException {
        return new ArticleSearch().searchByPhrase(isProximity, phrase)
                .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                .search();
    }

    public Map<String, Long> statsByPhrase(Boolean isProximity, String facetField, String author, String category, String phrase) throws SolrServerException, IOException {
        return new ArticleSearch().searchByPhrase(isProximity, phrase)
                .addCondition(new ExactFieldCondition(AUTHOR_FIELD).withValue(author))
                .addCondition(new ExactFieldCondition(CATEGORY_FIELD).withValue(category))
                .stats(facetField);
    }
}
