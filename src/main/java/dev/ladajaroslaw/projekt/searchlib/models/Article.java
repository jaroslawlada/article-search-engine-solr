package dev.ladajaroslaw.projekt.searchlib.models;

import org.apache.solr.client.solrj.beans.Field;

import java.util.List;

public class Article extends SearchModel {
    @Field
    String index;
    @Field
    String author;
    @Field
    String datePublished;
    @Field
    String category;
    @Field
    String section;
    @Field
    String url;
    @Field
    String headline;
    @Field
    String description;
    @Field
    String keywords;
    @Field
    String secondHeadline;
    @Field
    String language;
    @Field
    List<String> languages;
    @Field
    String articleText;

    public String getIndex() {
        return index;
    }

    public String getAuthor() {
        return author;
    }

    public String getDatePublished() {
        return datePublished;
    }

    public String getCategory() {
        return category;
    }

    public String getSection() {
        return section;
    }

    public String getUrl() {
        return url;
    }

    public String getHeadline() {
        return headline;
    }

    public String getDescription() {
        return description;
    }

    public String getKeywords() {
        return keywords;
    }

    public String getSecondHeadline() {
        return secondHeadline;
    }

    public String getLanguage() {
        return language;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public String getArticleText() {
        return articleText;
    }
}
