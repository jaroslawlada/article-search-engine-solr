package dev.ladajaroslaw.projekt.searchlib.search;

public class ExactFieldCondition extends SearchCondition {

    public ExactFieldCondition(String fieldName) {
        condition = fieldName + ":";
    }

    public ExactFieldCondition withValue(String value) {
        condition = (value == null) ? null : (condition + value);
        return this;
    }
}
