package dev.ladajaroslaw.projekt.searchlib.search;

import java.util.List;


public class KeyBuilder {

    public String generateKey(String word, boolean withTypo) {
        return withTypo ? word + "~" : word;
    }

    public String generateKey(List<String> words, boolean withTypo) {
        List<String> finalWords = words.stream()
                .map(w -> generateKey(w, withTypo))
                .toList();
        return String.join(" ", finalWords);
    }

    public String generatePhrase(String phrase, boolean withProximity) {
        String basicPhrase = "\"" + phrase + "\"";
        return withProximity ? basicPhrase + "~" : basicPhrase;
    }
}
