package dev.ladajaroslaw.projekt.searchlib.search;

import dev.ladajaroslaw.projekt.searchlib.models.SearchModel;
import org.apache.solr.client.solrj.SolrServerException;

import java.io.IOException;
import java.util.List;

public interface Search<T extends SearchModel> {
    boolean isTypoOn = false;
    Search simpleSearch(String key) throws SolrServerException, IOException;
    Search simpleSearch(String... keys) throws SolrServerException, IOException;
    List<T> search() throws SolrServerException, IOException;
}
