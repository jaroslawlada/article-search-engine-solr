package dev.ladajaroslaw.projekt.searchlib.search;

public abstract class SearchCondition {
    String condition;

    public String getCondition() {
        return condition == null ? null : "(" + condition + ")";
    }
}
