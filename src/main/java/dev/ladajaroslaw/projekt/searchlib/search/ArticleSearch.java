package dev.ladajaroslaw.projekt.searchlib.search;

import dev.ladajaroslaw.projekt.searchlib.SearchlibSorlClient;
import dev.ladajaroslaw.projekt.searchlib.models.Article;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.params.MapSolrParams;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ArticleSearch implements Search<Article> {
    HttpSolrClient client = SearchlibSorlClient.getClient();
    private final String COLLECTION = "articles";
    private boolean isTypoOn;
    private KeyBuilder keyBuilder = new KeyBuilder();
    private Map<String, String> queryParamsString = new HashMap<>();
    List<SearchCondition> conditions = new ArrayList<>();
    @Override
    public ArticleSearch simpleSearch(String key) throws SolrServerException, IOException {
        String finalKey = keyBuilder.generateKey(key, isTypoOn);
        queryParamsString.put("q", String.format("((author:%1$s) OR (description:%1$s) OR (articleText:%1$s) OR (headline:%1$s))", finalKey));
        return this;
    }

    @Override
    public ArticleSearch simpleSearch(String... keys) throws SolrServerException, IOException {
        String keysAsKey = keyBuilder.generateKey(Arrays.asList(keys), isTypoOn);
        queryParamsString.put("q", String.format("(author:(%1$s)) OR (description:(%1$s)) OR (articleText:(%1$s)) OR (headline:(%1$s))", keysAsKey));
        return this;
    }

    public ArticleSearch searchByPhrase(boolean isProximity, String phrase) {
        String finalPhrase = keyBuilder.generatePhrase(phrase, isProximity);
        queryParamsString.put("q", String.format("((headline:%1$s) OR (description:%1$s) OR (secondHeadline:%1$s) OR (articleText:%1$s))", finalPhrase));
        return this;
    }
    @Override
    public List<Article> search() throws SolrServerException, IOException {
        conditions.forEach(c -> {
            String newQ = queryParamsString.get("q") + " AND " + c.getCondition();
            queryParamsString.put("q", newQ);
        });
        MapSolrParams queryParams = new MapSolrParams(queryParamsString);
        QueryResponse response = client.query(COLLECTION, queryParams);
        return response.getBeans(Article.class);
    }

    public Map<String, Long> stats(String statsField) throws SolrServerException, IOException {
        conditions.forEach(c -> {
            String newQ = queryParamsString.get("q") + " AND " + c.getCondition();
            queryParamsString.put("q", newQ);
        });
        queryParamsString.put("facet.field", statsField);
        queryParamsString.put("facet", "true");

        MapSolrParams queryParams = new MapSolrParams(queryParamsString);
        QueryResponse response = client.query(COLLECTION, queryParams);
        return response.getFacetFields()
                .get(0)
                .getValues()
                .stream()
                .collect(Collectors.toMap(FacetField.Count::getName, FacetField.Count::getCount));
    }

    public ArticleSearch turnOnType() {
        isTypoOn = true;
        return this;
    }

    public ArticleSearch addCondition(SearchCondition condition) {
        if (condition.getCondition() != null) conditions.add(condition);
        return this;
    }
}
