package dev.ladajaroslaw.projekt.searchlib;

import org.apache.solr.client.solrj.impl.HttpSolrClient;

public class SearchlibSorlClient {
    final String SORL_URL = "http://localhost:8983/solr";
    private final HttpSolrClient client = buildClient();
    private static SearchlibSorlClient INSTANCE;

    private SearchlibSorlClient(){};

    public static HttpSolrClient getClient() {
        if (INSTANCE == null) {
            INSTANCE = new SearchlibSorlClient();
        }
        return INSTANCE.client;
    }

    private HttpSolrClient buildClient() {
        return new HttpSolrClient.Builder(SORL_URL)
                .withConnectionTimeout(10000)
                .withSocketTimeout(60000)
                .build();
    }
}
