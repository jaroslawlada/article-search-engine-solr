package dev.ladajaroslaw.projekt.config;

import dev.ladajaroslaw.projekt.searchlib.SearchlibSorlClient;
import org.apache.solr.client.solrj.SolrClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class SolrConfig {

    @Bean
    public SolrClient solrClient() {
        return SearchlibSorlClient.getClient();
    }
}
